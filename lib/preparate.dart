import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class Prep extends HiveObject {
  String name;
  String desc;
  List<Image> images = List.of([
    Image.network("https://wallpaperaccess.com/full/2637581.jpg"),
    Image.network("https://wallpaperaccess.com/full/2637581.jpg")
  ]);
  List<List<String>> imageButtonsMap;

  Prep(this.name, this.desc, this.imageButtonsMap, List<String> imagePaths) {
    for (final path in imagePaths) {
      images.add(Image.asset(path));
    }
  }
}
