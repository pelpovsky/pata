import 'package:flutter/material.dart';
import 'package:pata/preparate.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final List<String> entries = <String>['Общая паталогия', 'Частная паталогия'];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Pata',
        home: Scaffold(
            appBar: AppBar(
              title: Text('Pata: биопрепараты'),
            ),
            body: ListView.separated(
              padding: const EdgeInsets.all(8),
              itemCount: entries.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(entries[index]),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PreparateList(),
                      ),
                    );
                  },
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            )));
  }
}

class PreparateList extends StatelessWidget {
  final List<Prep> preparates = [
    Prep.Prep(
        name: 'Препарат 1',
        imagePath: 'assets/1.jpg',
        desc:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
    Prep.Prep(
        name: 'Препарат 2',
        imagePath: 'assets/2.jpg',
        desc:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
    Prep.Prep(
        name: 'Препарат 3',
        imagePath: 'assets/3.jpg',
        desc:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
    Prep.Prep(
        name: 'Препарат 4',
        imagePath: 'assets/4.jpg',
        desc:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
    Prep.Prep(
        name: 'Препарат 5',
        imagePath: 'assets/5.jpg',
        desc:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pata: биопрепараты'),
      ),
      body: ListView.builder(
        itemCount: preparates.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(preparates[index].name),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      PreparateDetail(preparate: preparates[index]),
                ),
              );
            },
          );
        },
      ),
    );
  }
}

class PreparateDetail extends StatefulWidget {
  final Prep preparate;

  const PreparateDetail({super.key, required this.preparate});

  @override
  _PreparateDetailState createState() => _PreparateDetailState();
}

class _PreparateDetailState extends State<PreparateDetail> {
  int _currentIndex = 0;
  final PageController _pageController =
      PageController(initialPage: 0, keepPage: false, viewportFraction: 0.5);

  @override
  dispose() {
    _pageController.dispose();
    super.dispose();
  }

  _showDescriptionScreen(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) {
          return DescriptionScreen(desc: widget.preparate.desc);
        },
      ),
    );
  }

  _builder(int index) {
    return AnimatedBuilder(
        animation: _pageController,
        builder: (context, child) {
          double value = 1.0;
          if (_pageController.position.haveDimensions) {
            value = _pageController.page! - index;
            value = (1 - (value.abs() * .5)).clamp(0.0, 1.0);
          }

          return Center(
            child: SizedBox(
              height: Curves.easeOut.transform(value) * 300,
              width: Curves.easeOut.transform(value) * 250,
              child: child,
            ),
          );
        },
        child: Image.asset(widget.preparate.imagePath));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.preparate.name),
          actions: [
            IconButton(
              icon: const Icon(Icons.info),
              onPressed: () {
                _showDescriptionScreen(context);
              },
            ),
          ],
        ),
        body: Center(
            child: Stack(alignment: Alignment.center, children: [
          PageView.builder(
              onPageChanged: (value) {
                setState(() {
                  _currentIndex = value;
                });
              },
              controller: _pageController,
              itemBuilder: (context, index) => _builder(index)),
          const Text('Увеличение такое-то'),
          Positioned(
              top: 100, // Adjust the top position as needed
              child: GestureDetector(
                  onTap: () {
                    // Handle button tap here
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: const Text('Уебищная клетка'),
                          content: const Text('И ее уебищное описание'),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: const Text('Назад'),
                            ),
                          ],
                        );
                      },
                    );
                  },
                  child: Container(
                    width: 50,
                    // Set the width of the circular button
                    height: 50,
                    // Set the height of the circular button
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.transparent,
                    ),
                    child: const Center(
                      child: Text(
                        '1',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 11.0,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ))),
          Positioned(
              top: 300,
              left: 50, // Adjust the top position as needed
              child: GestureDetector(
                  onTap: () {
                    // Handle button tap here
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: const Text('Уебищная клетка'),
                          content: const Text('И ее уебищное описание'),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: const Text('Назад'),
                            ),
                          ],
                        );
                      },
                    );
                  },
                  child: Container(
                    width: 50,
                    // Set the width of the circular button
                    height: 50,
                    // Set the height of the circular button
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.transparent,
                    ),
                    child: const Center(
                      child: Text(
                        '2',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 11.0,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  )))
        ])));
  }
}

class DescriptionScreen extends StatelessWidget {
  final String desc;

  const DescriptionScreen({super.key, required this.desc});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Описание'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: Text(
            desc,
            textAlign: TextAlign.left,
          ),
        ),
      ),
    );
  }
}
