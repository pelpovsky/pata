import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class Entry extends HiveObject {
  String name;
  List<Widget> insideItems;

  Entry(this.name, this.insideItems);
}
