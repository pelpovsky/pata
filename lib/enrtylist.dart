import 'package:flutter/material.dart';

class EntryList extends StatelessWidget {
  final Map<String, Widget> entries;

  const EntryList({super.key, required this.entries});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: entries.length,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          title: Text(entries.keys.toList(growable: false)[index]),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    entries[entries.keys.toList(growable: false)[index]]!,
              ),
            );
          },
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }
}
